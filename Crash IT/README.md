# Crash IT

Fun to play small Pygame based python game.
The objective of the game is to cross the river without
crashing into obstacles. There are three rounds. The
difficulty increases with each round.There is limited
time of fifteeen seconds to comlete each round. You get
points for how far you go in the round. You lose 150 points
for crashing into an obstacle. At the end of the
rounds the one with highest wins wins the game. In case of
a tie in wins score is matched and in case of tie in
score time is matched. Look out for the superpower in the
treasure chest. It grants you the ability to slow time.
Press X to use the power if you have it. The character of
the game can be controlled by the arrow keys for odd
numbered and WSAD keys for even numbered players.
Press P  during the game to pause it.
Press space on home to launch the game and press q to quit.

Author: Pratyanshu Pandey
