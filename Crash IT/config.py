import pygame

pygame.init()

# dimensions of different blocks to be displayed on display

screen_width = 1500
screen_height = 910
bar_height = 40
bar_width = screen_width
river_height = 100
river_width = screen_width
ground_height = 60
ground_width = screen_width
player_img_height = 60
player_img_width = 27
ship_height = 71
ship_width = 90
chest_width = 77
chest_height = 60

# colors

blue = (35, 172, 196)
ground_color = (115, 62, 22)
bar_color = (116, 61, 179)
fix_obs_color = (255, 0, 0)
crash_msg_color = (193, 35, 44)
charleston_green = (45, 44, 46)
chinese_silver = (199, 200, 202)

# fonts


small_text = pygame.font.SysFont('Arial', 60)
small_text2 = pygame.font.Font('Assassin$.ttf', 30)
medium_text = pygame.font.SysFont("Arial", 50)

# text messages to be displayed

control_text = "The objective of the game is to cross the river without " \
               "crashing into obstacles. There are three rounds. The" \
               " difficulty increases with each round.There si limited" \
               " time of fifteeen seconds to comlete each round. You get" \
               " points for how far you go in the round. At the end of the" \
               " rounds the one with highest wins wins the game. In case of" \
               " a tie in wins score is matched and in case of tie in" \
               " score time is matched. Look out for the superpower in the" \
               " treasure chest. It grants you the ability to slow time. " \
               "Press X to use the power if you have it. The character of " \
               "the game can be controlled by the arrow keys for odd " \
               "numbered and WSAD keys for even numbered players." \
               " Press P during the game to pause it." \
               " Press space on home to launch the game and press q to quit."
success_text = "Congratulations on your victory"
crash_text = "OOPS you crashed"

# keys to convert number to words
player_key = [ "zero", "one", "two"]
round_key = ["one", "one", "one", "two", "two", "three", "three"]
