import time
import random
from config import *

pygame.init()

# Here are declared those global variables that are to be used
# throughout the programme.

gameDisplay = pygame.display.set_mode((screen_width, screen_height))
clock = pygame.time.Clock()
pygame.display.set_caption('Crash It')
player1_img = pygame.image.load('_p1.png')
player2_img = pygame.image.load('_p1.png')
back_img = pygame.image.load('background2.jpg')
cloud = pygame.image.load('cloud.jpg')
fireball = pygame.image.load('_fireball2.png')
pirate = pygame.image.load('_pirateship.png')
chest = pygame.image.load('_treasure.png')
ground = pygame.image.load('desert.jpg')
cactus = pygame.image.load('_cactus.png')
river = pygame.image.load('_river.jpg')
pause = False
round = 0
cur = 0


# This function is to check if player crashes with the given  obstacle.

def iscrashed(p_x, p_y, obs_x, obs_y, obs_height, obs_width):
    if p_x < obs_x < p_x + player_img_width \
            and p_y < obs_y < p_y + player_img_width:
        return True
    if p_x < obs_x + obs_width < p_x + player_img_width \
            and p_y < obs_y < p_y + player_img_width:
        return True
    if obs_x < p_x < obs_x + obs_width \
            and obs_y < p_y < obs_y + obs_height:
        return True
    if p_x < obs_x < p_x + player_img_width \
            and obs_y < p_y < obs_y + obs_height:
        return True
    return False


# This is function for displaying time while running the game.

def time_display(time):
    time //= 60
    if time > 15:
        time_up()
    else:
        time = 'Time: ' + str(time)
        text_surf, text_rect = text_objects(time, small_text, crash_msg_color)
        text_rect.center = (1350, 40)
        gameDisplay.blit(text_surf, text_rect)


# This function is for displaying the score while running the game.

def display_score(score):
    score = 'Score:' + str(score)
    text_surf, text_rect = text_objects(score, small_text, crash_msg_color)
    text_rect.center = (130, 30)
    gameDisplay.blit(text_surf, text_rect)


# This function creates text surfaces to write on.

def text_objects(text, font, color):
    text_surface = font.render(text, True, color)
    return text_surface, text_surface.get_rect()


# This function displays text at the center of the screen.

def message_display(text):
    text = str(text)
    large_text = pygame.font.Font('Assassin$.ttf', 115)
    text_surf, text_rect = text_objects(text, large_text, crash_msg_color)
    text_rect.center = ((screen_width / 2), (screen_height / 2))
    time.sleep(0.1)
    gameDisplay.blit(cloud, (0, 0))
    gameDisplay.blit(text_surf, text_rect)
    pygame.display.update()
    time.sleep(1)


# This function is for creating a button at a certain position
# with a certain color scheme that performs a certain action.

def button(text, x, y, w, h, ic, ac, text_color, action=None):
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    if x + w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(gameDisplay, ac, (x, y, w, h))

        if click[0] == 1 and action is not None:
            action()
    else:
        pygame.draw.rect(gameDisplay, ic, (x, y, w, h))

    text_surf, text_rect = text_objects(text, small_text2, text_color)
    text_rect.center = ((x + (w / 2)), (y + (h / 2)))
    gameDisplay.blit(text_surf, text_rect)


# This function is for resetting the data after each round.

def reset():
    player = playerlist[cur]
    player.total_time += player.time
    player.x = screen_width * 0.45
    if player.number == 1:
        player.y = screen_height - player_img_height
    else:
        player.y = 0
    player.total_score += player.score
    player.time = 0
    player.score = 0
    if player.use_power:
        player.use_power = False
        player.prize -= 1
    player.x_change = 0
    player.y_change = 0


# This function is for resetting the data after 3 rounds.

def hard_reset():
    reset()
    global cur
    cur = (cur + 1) % 2
    reset()
    for player in playerlist:
        player.total_score = 0
        player.total_time = 0
        player.prize = 0
        player.win = 0
        player.use_power = False


# This function is called to display message and continue the
# game if crash occurs.

def crashed():
    message_display(crash_text)
    reset()
    global cur
    cur = (cur + 1) % 2
    game_loop(playerlist[cur])


# This function is for displaying a message that time is up
# and to continue the game.

def time_up():
    message_display("Time Up!")
    reset()
    global cur
    cur = (cur + 1) % 2
    game_loop(playerlist[cur])


# This function displays victory message and continues the game.

def win(num):
    gameDisplay.blit(cloud, (0, 0))
    message_display("Player " + num + " Wins")
    reset()
    global cur
    playerlist[cur].win += 1
    cur = (cur + 1) % 2
    game_loop(playerlist[cur])


# This function is for exiting the game.

def game_exit():
    pygame.quit()
    quit()


# This function loads the pause page when game is paused.

def pause_page():
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    return
                if event.key == pygame.K_q:
                    game_exit()

        gameDisplay.blit(cloud, (0, 0))
        large_text = pygame.font.Font('Assassin$.ttf', 170)
        text_surf, text_rect = text_objects("Game Paused", large_text,
                                            crash_msg_color)
        text_rect.center = (screen_width // 2, screen_height // 4 + 10)
        gameDisplay.blit(text_surf, text_rect)
        text_surf, text_rect = text_objects(
            "press R to continue Q to exit game",
            medium_text, charleston_green)
        text_rect.center = (screen_width * 0.45, screen_height // 2)
        gameDisplay.blit(text_surf, text_rect)
        button('QUIT', screen_width * 0.45, 3 / 4 * screen_height, 150, 60,
               chinese_silver, charleston_green, crash_msg_color, game_exit)
        pygame.display.update()
        clock.tick(60)


# Declared here is a class for moving obstacles i.e. the fireballs.

class MovObs:
    def __init__(self, x_pos, y_pos, speed):
        self.x = x_pos
        self.y = y_pos
        self.speed = speed
        self.height = 40
        self.width = 38

    # function to display the moving obstacle

    def draw(self):
        gameDisplay.blit(fireball, (self.x, self.y))


# Declared here is a class for ships.

class Ship:
    def __init__(self, x_pos, y_pos, speed):
        self.x = x_pos
        self.y = y_pos
        self.speed = speed

    # function to display the ship

    def draw(self):
        gameDisplay.blit(pirate, (self.x, self.y))


# Declared here is a class for players.

class Players:
    def __init__(self, num, p_x, p_y, p_img):
        self.number = num
        self.x = p_x
        self.y = p_y
        self.x_change = 0
        self.y_change = 0
        self.img = p_img
        self.score = 0
        self.time = 0
        self.total_score = 0
        self.total_time = 0
        self.x_change = 0
        self.y_change = 0
        self.win = 0
        self.prize = 0
        self.use_power = False
        if self.number == 1:
            self.up = pygame.K_UP
            self.down = pygame.K_DOWN
            self.left = pygame.K_LEFT
            self.right = pygame.K_RIGHT
        else:
            self.up = pygame.K_w
            self.down = pygame.K_s
            self.left = pygame.K_a
            self.right = pygame.K_d

    # function to display player

    def player_load(self):
        gameDisplay.blit(self.img, (self.x, self.y))

    # function to track movement of player

    def player_mov(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_p:
                    pause_page()
                elif event.key == self.left:
                    self.x_change = -5
                elif event.key == self.right:
                    self.x_change = 5
                elif event.key == self.up:
                    self.y_change = -5
                elif event.key == self.down:
                    self.y_change = 5
                elif event.key == pygame.K_x and self.prize > 0:
                    self.use_power = True
            if event.type == pygame.KEYUP:
                if event.key == self.left or event.key == self.right:
                    self.x_change = 0
                elif event.key == self.up or event.key == self.down:
                    self.y_change = 0

        self.x += self.x_change
        self.y += self.y_change
        if self.x + player_img_width > screen_width:
            self.x = screen_width - player_img_width
        if self.x < 0:
            self.x = 0
        if self.y + player_img_height > screen_height:
            self.y = screen_height - player_img_height
        if self.y < 0:
            self.y = 0
        if 0 < self.y < screen_height - player_img_height and self.number == 1:
            self.score -= self.y_change
        if 0 < self.y < screen_height - player_img_height and self.number == 2:
            self.score += self.y_change


# This function is called to load intro page at the beginning of the game.

def intro_page():
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    game()
                if event.key == pygame.K_q:
                    game_exit()

        gameDisplay.blit(back_img, (0, 0))
        large_text = pygame.font.Font('Assassin$.ttf', 170)
        text_surf, text_rect = text_objects("IT!", large_text, crash_msg_color)
        text_rect.center = (screen_width // 2, screen_height // 4 + 10)
        gameDisplay.blit(text_surf, text_rect)
        text_surf, text_rect = text_objects("Crash",
                                            large_text, charleston_green)
        text_rect.center = (screen_width * 0.45, screen_height // 6)
        gameDisplay.blit(text_surf, text_rect)
        button('PLAY', 300, 3 / 4 * screen_height, 150, 60, chinese_silver,
               charleston_green, crash_msg_color, game)
        button('CONTROLS', 600, 3 / 4 * screen_height, 200, 60,
               chinese_silver, charleston_green, crash_msg_color, cont)
        button('QUIT', 900, 3 / 4 * screen_height, 150, 60, chinese_silver,
               charleston_green, crash_msg_color,
               game_exit)
        pygame.display.update()
        clock.tick(15)
        pygame.display.update()


# This function announces the final result after 3 rounds.

def game_result():
    gameDisplay.blit(cloud, (0, 0))
    if playerlist[0].win > playerlist[1].win:
        message_display("Player ONE is the winner")
    elif playerlist[0].win < playerlist[1].win:
        message_display("Player TWO is the winner")
    elif playerlist[0].total_score > playerlist[1].total_score:
        message_display("Player ONE is the winner")
    elif playerlist[0].total_score > playerlist[1].total_score:
        message_display("Player TWO is the winner")
    elif playerlist[0].total_time < playerlist[1].total_time:
        message_display("Player ONE is the winner")
    else:
        message_display("Player TWO is the winner")
    message_display(success_text)
    hard_reset()
    time.sleep(1)
    intro_page()


# Declared here are global variables used inside main game loop.

p1 = Players(1, screen_width * 0.45, screen_height - player_img_height,
             player1_img)
p2 = Players(2, screen_width * 0.45, 0, player2_img)
playerlist = list()
playerlist.append(p1)
playerlist.append(p2)


# This function contains the main loop that runs till game ends and
# decides what to display at each frame.

def game_loop(player):
    global round
    round += 1
    if round == 7:
        game_result()
    message_display("Round " + round_key[round] +
                    ' Player ' + player_key[player.number])
    clock_count = 0
    adder = round // 2
    fix_obs = []
    for i in range(bar_height + ground_height + river_height,
                   screen_height - ground_height - 10,
                   river_height + ground_height):
        num = random.randrange(4, 8)
        for k in range(num):
            fix_obs.append((random.randrange(0, screen_width), i))
    mov_obs = []
    ship_list = []
    luck = (random.randrange(0, screen_width - ground_height),
            random.randrange(bar_height + ground_height,
                             screen_height - ground_height - river_height,
                             river_height + ground_height) + 20)
    for i in range(bar_height + ground_height,
                   screen_height - ground_height - river_height,
                   river_height + ground_height):
        sh = Ship(random.randrange(-100, 1600), i + 20,
                  random.randrange(-3, 4, 2) + adder)
        ship_list.append(sh)
    crash = 0
    obtained = False

    while True:  # the main game loop
        player.time += 1
        player.player_mov()
        gameDisplay.blit(ground, (0, 0))
        for i in range(bar_height + ground_height, screen_height - 10,
                       river_height + ground_height):
            gameDisplay.blit(river, (0, i))
        for tup in fix_obs:
            x, y = tup
            gameDisplay.blit(cactus, (x, y))

            if iscrashed(player.x, player.y, x, y, ground_height,
                         ground_height):
                player.score -= 150
                crash = 1
        if not obtained:
            gameDisplay.blit(chest, (luck[0], luck[1]))

        for obj in mov_obs:
            obj.x += obj.speed + adder * 2
            if obj.x > screen_width or obj.speed == 0:
                mov_obs.remove(obj)
            else:
                obj.draw()
            if iscrashed(player.x, player.y, obj.x, obj.y, obj.height,
                         obj.width):
                crash = 1

        if iscrashed(player.x, player.y, luck[0], luck[1],
                     chest_height, chest_width):
            player.prize += 1
            obtained = True

        for sh in ship_list:
            sh.x += sh.speed
            if sh.x > screen_width:
                sh.x = -100
                sh.speed = random.randrange(3, 5)
            sh.draw()
            if iscrashed(player.x, player.y, sh.x, sh.y,
                         ship_height, ship_width):
                crash = 1
            if player.time % 90 == 0:
                num = random.randrange(2, 4) + adder * 3
                for i in range(num):
                    obj = MovObs(sh.x, sh.y,
                                 random.randrange(-8 - adder * 2,
                                                  8 + adder * 2))
                    if obj.speed != sh.speed:
                        mov_obs.append(obj)

        time_display(player.time)
        display_score(player.score)
        player.player_load()
        if crash == 1:
            crashed()
        if player.number == 1 and player.y == 0:
            win('One')
        elif player.number == 2 and \
                player.y == screen_height - player_img_height:
            win('Two')
        pygame.display.update()
        if not player.use_power:
            clock.tick(60)
        else:
            clock_count += 1
            clock.tick(20)
            if clock_count == 60:
                player.use_power = False
                player.prize -= 1
                clock_count = 0


# This function starts the game from intro page.

def game():
    global round
    round = 0
    global cur
    cur = 0
    game_loop(playerlist[0])


# This function displays the input text in separate lines.

def blit_text(text, pos, font, color):
    words = [word.split(' ') for word in text.splitlines()]
    space = font.size(' ')[0]
    max_width, max_height = gameDisplay.get_size()
    x, y = pos
    for line in words:
        for word in line:
            word_surface = font.render(word, 0, color)
            word_width, word_height = word_surface.get_size()
            if x + word_width >= max_width - 40:
                x = pos[0]
                y += word_height
            gameDisplay.blit(word_surface, (x, y))
            x += word_width + space
        x = pos[0]
        y += word_height


# This function is called to display the controls page.

def cont():
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    game_exit()
        gameDisplay.blit(cloud, (0, 0))
        large_text = pygame.font.Font('Assassin$.ttf', 90)
        text_surf, text_rect = text_objects("Controls", large_text,
                                            crash_msg_color)
        text_rect.center = (screen_width // 2, 40)
        gameDisplay.blit(text_surf, text_rect)
        blit_text(control_text, (40, 100), medium_text, charleston_green)
        button('Back', screen_width * 0.45, 800, 150, 60, chinese_silver,
               charleston_green, crash_msg_color, intro_page)
        pygame.display.update()


# Calling the function that loads the home page
# of the game.

intro_page()
